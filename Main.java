import java.util.*;
import java.io.*;

/**
 * Pokemon main class
 */
public class Main extends Engine{
    Character player = new Character();
    ArrayList<Pokemon> allPokemonDB = new ArrayList<Pokemon>();
    ArrayList<Pokemon> allPokemon = new ArrayList<Pokemon>();
    ArrayList<Character> allEnemyLevel1 = new ArrayList<Character>();
    ArrayList<Character> allEnemyLevel2 = new ArrayList<Character>();
    ArrayList<Character> allEnemyLevel3 = new ArrayList<Character>();
    PokeCenter pokeCenter = new PokeCenter();

    public static void main (String[] args) {
        Main game = new Main();

        game.loadFromFilePokemon(game);
        game.createEnemy(game);

        game.intro(game);
        // game.createPlayer(game);
        game.menu(game);

        // Demo battle sequence
        // String starter = game.rand.nextBoolean() ? "player" : "enemy";
        // boolean statusWinning = game.battleSequence(game.allEnemyLevel1.get(2), starter, game);
        // System.out.println(statusWinning);


    }
}
/**
 *
Lose
========================================
Case 1:
pilih: 1
first pokemon battle: 1
error
Case 2:
pilih: 1
first pokemon battle: 2
error
Case 3:
pilih: 2
first pokemon battle: 1
normal
Case 4:
pilih: 2
first pokemon battle: 2
normal
========================================
ketika kita punya 2 pokemon dengan nama yang sama maka ketika 1 kalah maka yang lain hpnya ikut pada yang kalah.
hipotesis: error terletak pada pengaksesan pokemon karena yang dioperke parameter attak adalah sama nama objectnya sehingga dia merubah semua value nama pbject yang sama pada allPokemon
----------------------------------------
 */
