import java.util.*;

class PokeCenter extends Tools {

	ArrayList<String> hpPotions = new ArrayList<>();
	ArrayList<String> staPotions = new ArrayList<>();

	void healAll(Main game) {

		for (Pokemon m : game.player.myPokemon) {
				for (Pokemon n : game.allPokemonDB) {
						if (m.name.equals(n.name)) {
								m.hp = n.hp;
								m.energy = n.energy;
						}
				}
		}
		delayedCharPrint(String.format("......................."), 100);
		System.out.println("DONE, All of your Pokemon Healed");

	}

	int jumlahHPPotion() {
		return hpPotions.size();
	}

	int jumlahStaPotion() {
		return staPotions.size();
	}

	void buyHPPotion(Main game) {
		hpPotions.add("HPPotion");
		game.player.money -= 1000;
		delayedCharPrint("HP Potion bought!\n", 20);
	}

	void buyStaPotion(Main game) {
		staPotions.add("StaPotion");
		game.player.money -= 500;
		delayedCharPrint("STA Potion bought!\n", 20);
	}

	void usePotion(Pokemon p, int potion) {
		if (potion == 1) {
			if (jumlahHPPotion() != 0) {
				hpPotions.remove(0);
				p.hp += 15;
				delayedCharPrint("HP Potion used!\n", 20);
			} else {
				delayedCharPrint("You have no HP potion!\n", 20);
			}
		} else if (potion == 2) {
			if (jumlahStaPotion() != 0) {
				staPotions.remove(0);
				p.energy += 10;
				delayedCharPrint("Potion used!\n", 20);
			} else {
				delayedCharPrint("You have no STA potion!\n", 20);
			}
		}

	}

}
